#!/usr/bin/env bash
: <<COMMENTBLOCK
title			:Install ohMyZSH + aliaces
description	:
author			:Valeriu Stinca
email			:ts@strategic.zone
date			:20200515
version			:0.4
notes			:
================================================
COMMENTBLOCK


set -e

#check if commandes exists
if ! [ -x "$(command -v git)" ]; then
  echo 'Error: please install git.' >&2
  exit 1
fi
if ! [ -x "$(command -v zsh)" ]; then
	echo 'Error: please install zsh.' >&2
	exit 1
fi

# Creation config de zsh avec oh-my-zsh
git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
if [ -f ~/.zshrc ]; then
	cp ~/.zshrc ~/.zshrc.orig
fi
touch ~/.zshrc
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
sed -i 's/^ZSH_THEME.*$/ZSH_THEME="agnoster"/' ~/.zshrc
sed -i 's/\#\ ENABLE_CORRECTION=\"true\"/ENABLE_CORRECTION=\"true\"/g' ~/.zshrc
if [ ! -f ~/.aliasrc ]; then
	touch ~/.aliasrc
fi
echo 'alias tb="nc termbin.com 9999"' | tee -a ~/.aliasrc
echo 'alias tmt="_tmt() { tar -c --use-compress-program=pigz -f \"${1}_$(date +%F_%H%M%S).txz\" \"$1\"; }; _tmt"' | tee -a ~/.aliasrc
echo 'source ~/.aliasrc' | tee -a ~/.zshrc
sudo chsh  $USER -s /bin/zsh 
echo "Your default SHELL changed with: chsh $USER -s /bin/zsh"
